import 'package:flutter/cupertino.dart';

class Preference {
  static const USER_INFO = "user-info";
}

class Api {
  static const BASE_URL = "";
  static const LOGIN = "/login";
  static const REGISTER = "/register";
}

class ColorTheme {
  static const backgroundColor = Color(0xFF272523);
  static const primaryColor = Color(0xFFFFA51E);
}

class ScreenUtilConstants {
  static const width = 320.0;
  static const height = 640.0;
}
