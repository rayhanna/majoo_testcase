import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/bloc/auth_bloc/auth_bloc_cubit.dart';
import 'package:majootestcase/models/movie_response.dart';
import 'package:majootestcase/ui/common/widgets/custom_button.dart';
import 'package:majootestcase/ui/detail_movie/detail_movie_screen.dart';
import 'package:majootestcase/utils/constant.dart';
import 'package:majootestcase/utils/page_transition_builder.dart';

import '../../main.dart';

class HomeSliverAppBar extends StatelessWidget {
  final Data data;
  const HomeSliverAppBar(this.data, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SliverAppBar(
      pinned: true,
      expandedHeight: 250.0,
      backgroundColor: ColorTheme.backgroundColor,
      title: Row(
        children: [
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                RichText(
                  text: TextSpan(
                    text: 'Welcome aboard',
                    style: TextStyle(
                      color: Colors.white.withOpacity(0.7),
                    ),
                    children: [
                      TextSpan(
                        text: ' 👋',
                        style: TextStyle(
                          color: Colors.white,
                        ),
                      )
                    ],
                  ),
                ),
                const SizedBox(height: 4),
                Text(
                  'Let\'s relax and watch a movie!',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 14,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ],
            ),
          ),
          IconButton(
            onPressed: () {
              showDialog(
                context: context,
                builder: (context) {
                  return AlertDialog(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(12),
                    ),
                    backgroundColor: ColorTheme.backgroundColor,
                    title: const Text('Log Out'),
                    content: const Text('Are you sure want to logging out?'),
                    actionsPadding: const EdgeInsets.all(20),
                    actions: [
                      Row(
                        children: [
                          Expanded(
                            child: CustomButton(
                              text: 'No',
                              onPressed: () => Navigator.of(context).pop(),
                            ),
                          ),
                          const SizedBox(width: 20),
                          Expanded(
                            child: BlocBuilder<AuthBlocCubit, AuthBlocState>(
                              bloc: AuthBlocCubit(),
                              builder: (context, state) {
                                return CustomButton(
                                  text: 'Yes',
                                  isSecondary: true,
                                  onPressed: () async {
                                    await AuthBlocCubit().logOut();
                                    Navigator.pushAndRemoveUntil(
                                      context,
                                      CustomRoute.createRoute(
                                        page: BlocProvider(
                                          create: (context) => AuthBlocCubit()
                                            ..fetchHistoryLogin(),
                                          child: MyHomePageScreen(),
                                        ),
                                      ),
                                      (Route<dynamic> route) => false,
                                    );
                                  },
                                );
                              },
                            ),
                          ),
                        ],
                      )
                    ],
                  );
                },
              );
            },
            icon: Icon(
              Icons.logout_rounded,
              color: Colors.white,
            ),
          ),
        ],
      ),
      flexibleSpace: FlexibleSpaceBar(
        background: Stack(
          children: [
            Positioned.fill(
              child: Image.network(
                data.i!.imageUrl!,
                fit: BoxFit.cover,
              ),
            ),
            Positioned(
              top: 0,
              left: 0,
              right: 0,
              child: Container(
                height: 100,
                width: double.infinity,
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    colors: [
                      ColorTheme.backgroundColor,
                      Colors.transparent,
                    ],
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                  ),
                ),
              ),
            ),
            Positioned(
              bottom: 0,
              left: 0,
              right: 0,
              child: Container(
                height: 50,
                width: double.infinity,
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    colors: [
                      ColorTheme.backgroundColor,
                      Colors.transparent,
                    ],
                    end: Alignment.topCenter,
                    begin: Alignment.bottomCenter,
                  ),
                ),
              ),
            ),
            Positioned(
              left: 12,
              right: 12,
              bottom: 24,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    data.l!,
                    style: TextStyle(
                      fontSize: 24,
                      fontWeight: FontWeight.bold,
                      shadows: [
                        Shadow(
                          color: Colors.black,
                          offset: Offset(0.0, 4.0),
                          blurRadius: 8.0,
                        ),
                      ],
                    ),
                  ),
                  const SizedBox(height: 12),
                  SizedBox(
                    height: 40,
                    width: 150,
                    child: CustomButton(
                      text: 'See Details',
                      leadingIcon: Icon(
                        Icons.info_outline_rounded,
                        color: Colors.white,
                      ),
                      onPressed: () {
                        Navigator.of(context).push(
                          CustomRoute.createRoute(
                              page: DetailMovieScreen(data)),
                        );
                      },
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
