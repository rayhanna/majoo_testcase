import 'package:flutter/material.dart';
import 'package:majootestcase/ui/common/widgets/custom_button.dart';
import 'package:majootestcase/utils/constant.dart';

class ErrorScreen extends StatelessWidget {
  final String message;
  final Function()? retry;
  final Color? textColor;
  final double fontSize;
  final double gap;
  final Widget? retryButton;

  const ErrorScreen(
      {Key? key,
      this.gap = 10,
      this.retryButton,
      this.message = "",
      this.fontSize = 14,
      this.retry,
      this.textColor})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Icon(
              Icons.error_outline_rounded,
              color: Colors.white,
              size: 80,
            ),
            const SizedBox(
              height: 12,
            ),
            Text(
              message,
              style: TextStyle(fontSize: 14, color: textColor ?? Colors.white),
            ),
            const SizedBox(
              height: 36,
            ),
            retry != null
                ? retryButton ??
                    CustomButton(
                      text: 'Refresh',
                      height: 40,
                      width: 150,
                      isSecondary: true,
                      onPressed: () {
                        if (retry != null) retry!();
                      },
                      leadingIcon: Icon(
                        Icons.refresh_sharp,
                        color: ColorTheme.primaryColor,
                      ),
                    )
                : SizedBox()
          ],
        ),
      ),
    );
  }
}
