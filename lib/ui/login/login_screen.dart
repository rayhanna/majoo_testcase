import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/bloc/auth_bloc/auth_bloc_cubit.dart';
import 'package:majootestcase/bloc/home_bloc/home_bloc_cubit.dart';
import 'package:majootestcase/ui/common/widgets/custom_button.dart';
import 'package:majootestcase/ui/home/home_bloc_screen.dart';
import 'package:majootestcase/ui/login/register_screen.dart';
import 'package:majootestcase/ui/login/sign_in_screen.dart';
import 'package:majootestcase/utils/constant.dart';
import 'package:majootestcase/utils/page_transition_builder.dart';

class LoginScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocListener<AuthBlocCubit, AuthBlocState>(
        listener: (context, state) {
          if (state is AuthBlocLoggedInState) {
            Navigator.pushAndRemoveUntil(
                context,
                CustomRoute.createRoute(
                  page: BlocProvider(
                    create: (context) => HomeBlocCubit()..fetchingData(),
                    child: HomeBlocScreen(),
                  ),
                ),
                (Route<dynamic> route) => false);
          }
        },
        child: SingleChildScrollView(
          child: Container(
            height: MediaQuery.of(context).size.height,
            padding: EdgeInsets.symmetric(horizontal: 20, vertical: 28),
            child: SafeArea(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  const Text(
                    'Sign In, and',
                    style: TextStyle(
                      fontSize: 15,
                      fontWeight: FontWeight.w400,
                    ),
                  ),
                  const SizedBox(height: 4),
                  const Text(
                    'Let’s watch movie!',
                    style: TextStyle(
                      fontSize: 24,
                      fontWeight: FontWeight.bold,
                      // color: colorBlue,
                    ),
                  ),
                  const SizedBox(height: 36),
                  Expanded(
                    child: Center(
                      child: Image.asset('assets/images/img-login.png'),
                    ),
                  ),
                  const SizedBox(height: 36),
                  CustomButton(
                    text: 'Sign In',
                    onPressed: () {
                      Navigator.of(context).push(
                        CustomRoute.createRoute(
                          page: BlocProvider(
                            create: (context) => AuthBlocCubit(),
                            child: SignInScreen(),
                          ),
                          isVertical: true,
                        ),
                      );
                    },
                  ),
                  const SizedBox(height: 12),
                  CustomButton(
                    text: 'Register',
                    isSecondary: true,
                    onPressed: () {
                      Navigator.of(context).push(
                        CustomRoute.createRoute(
                          page: BlocProvider(
                            create: (context) => AuthBlocCubit(),
                            child: RegisterScreen(),
                          ),
                          isVertical: true,
                        ),
                      );
                    },
                  ),
                  const SizedBox(height: 20),
                  RichText(
                    text: TextSpan(
                      text: 'By clicking to continue, you\'re agreeing to our',
                      style: TextStyle(
                        fontSize: 12,
                      ),
                      children: [
                        TextSpan(
                          text: ' Terms and Condition',
                          style: TextStyle(
                            color: ColorTheme.primaryColor,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        TextSpan(text: ' and'),
                        TextSpan(
                          text: ' Condition',
                          style: TextStyle(
                            color: ColorTheme.primaryColor,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ],
                    ),
                    textAlign: TextAlign.center,
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
