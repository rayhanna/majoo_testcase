import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/bloc/auth_bloc/auth_bloc_cubit.dart';
import 'package:majootestcase/bloc/home_bloc/home_bloc_cubit.dart';
import 'package:majootestcase/models/user.dart';
import 'package:majootestcase/ui/common/widgets/custom_appbar.dart';
import 'package:majootestcase/ui/common/widgets/custom_button.dart';
import 'package:majootestcase/ui/common/widgets/text_form_field.dart';
import 'package:majootestcase/ui/home/home_bloc_screen.dart';
import 'package:majootestcase/ui/login/sign_in_screen.dart';
import 'package:majootestcase/utils/constant.dart';
import 'package:majootestcase/utils/error_helper.dart';
import 'package:majootestcase/utils/page_transition_builder.dart';

class RegisterScreen extends StatefulWidget {
  @override
  _RegisterState createState() => _RegisterState();
}

class _RegisterState extends State<RegisterScreen> {
  GlobalKey<FormState> _formKey = new GlobalKey<FormState>();
  final _emailController = TextController();
  final _passwordController = TextController();
  final _confirmPasswordController = TextController();

  Map<String, String> _authData = {};

  bool _isObscurePassword = true;
  bool _isObscurePasswordConfirm = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar('Register'),
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.all(24),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              _form(),
              const SizedBox(
                height: 50,
              ),
              BlocBuilder<AuthBlocCubit, AuthBlocState>(
                bloc: context.read<AuthBlocCubit>(),
                builder: (context, state) {
                  if (state is AuthBlocErrorState) {
                    ErrorHelper.showSnackBarError(
                        context, state.props[0].toString());
                    context.read<AuthBlocCubit>().resetState();
                  }

                  if (state is AuthBlocLoggedInState) {
                    handleGoToHome();
                  }

                  final isLoading = state is AuthBlocLoadingState;

                  return CustomButton(
                    text: 'Register',
                    onPressed: handleRegister,
                    isLoading: isLoading,
                  );
                },
              ),
              const SizedBox(
                height: 50,
              ),
              _register(),
            ],
          ),
        ),
      ),
    );
  }

  Widget _form() {
    return Form(
      key: _formKey,
      child: Column(
        children: [
          CustomTextFormField(
            context: context,
            isEmail: true,
            controller: _emailController,
            hint: 'Example@123.com',
            label: 'Email',
            validator: (val) {
              final pattern = RegExp(r'([\d\w]{1,}@[\w\d]{1,}\.[\w]{1,})');
              if (val != null)
                return pattern.hasMatch(val) ? null : 'Email is invalid';
            },
            onSaved: (value) {
              setState(() {
                _authData['email'] = value!;
              });
            },
          ),
          const SizedBox(height: 12),
          CustomTextFormField(
            context: context,
            label: 'Password',
            hint: 'password',
            controller: _passwordController,
            isObscureText: _isObscurePassword,
            suffixIcon: IconButton(
              icon: Icon(
                _isObscurePassword
                    ? Icons.visibility_off_outlined
                    : Icons.visibility_outlined,
              ),
              onPressed: () {
                setState(() {
                  _isObscurePassword = !_isObscurePassword;
                });
              },
            ),
            validator: (val) {
              if (val != null)
                return val.length < 6 ? 'Password is too short' : null;
            },
            onSaved: (value) {
              setState(() {
                _authData['password'] = value!;
              });
            },
          ),
          const SizedBox(height: 12),
          CustomTextFormField(
            context: context,
            label: 'Password',
            hint: 'password',
            controller: _confirmPasswordController,
            isObscureText: _isObscurePasswordConfirm,
            suffixIcon: IconButton(
              icon: Icon(
                _isObscurePasswordConfirm
                    ? Icons.visibility_off_outlined
                    : Icons.visibility_outlined,
              ),
              onPressed: () {
                setState(() {
                  _isObscurePasswordConfirm = !_isObscurePasswordConfirm;
                });
              },
            ),
            validator: (val) {
              if (val != null)
                return val == _passwordController.textController.text
                    ? null
                    : 'Password did not match';
            },
            onSaved: (value) {
              setState(() {
                _authData['password'] = value!;
              });
            },
          ),
        ],
      ),
    );
  }

  Widget _register() {
    return Align(
      alignment: Alignment.center,
      child: TextButton(
        onPressed: () {
          Navigator.of(context).push(
            CustomRoute.createRoute(
              page: BlocProvider(
                create: (context) => AuthBlocCubit(),
                child: SignInScreen(),
              ),
              isVertical: true,
            ),
          );
        },
        child: RichText(
          text: TextSpan(
            text: 'Already have an account?',
            children: [
              TextSpan(
                text: ' Sign In',
                style: TextStyle(
                  color: ColorTheme.primaryColor,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void handleRegister() async {
    if (!_formKey.currentState!.validate()) {
      return;
    }
    _formKey.currentState!.save();

    User user = User.fromJson(_authData);
    context.read<AuthBlocCubit>().createUser(user);
  }

  void handleGoToHome() {
    WidgetsBinding.instance?.addPostFrameCallback((_) {
      Navigator.pushAndRemoveUntil(
          context,
          CustomRoute.createRoute(
            page: BlocProvider(
              create: (context) => HomeBlocCubit()..fetchingData(),
              child: HomeBlocScreen(),
            ),
          ),
          (Route<dynamic> route) => false);
    });
  }
}
