import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:majootestcase/db/db_helper.dart';
import 'package:majootestcase/models/user.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'auth_bloc_state.dart';

class AuthBlocCubit extends Cubit<AuthBlocState> {
  AuthBlocCubit() : super(AuthBlocInitialState());

  void fetchHistoryLogin() async {
    print('fetchHistoryLogin');
    emit(AuthBlocInitialState());
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    bool? isLoggedIn = sharedPreferences.getBool("is_logged_in");
    if (isLoggedIn == null) {
      emit(AuthBlocLoginState());
    } else {
      if (isLoggedIn) {
        emit(AuthBlocLoggedInState());
      } else {
        emit(AuthBlocLoginState());
      }
    }
  }

  void resetState() {
    emit(AuthBlocInitialState());
  }

  void loginUser(User user) async {
    emit(AuthBlocLoadingState());
    final users = await DBHelper.getData('user_auth');
    final usersEmail = users.map((userData) => userData['email']);

    if (!usersEmail.contains(user.email)) {
      emit(AuthBlocErrorState('User with this email is not registered'));
      return;
    }

    if (users.where((userData) => userData['email'] == user.email).toList()[0]
            ['password'] !=
        user.password) {
      emit(AuthBlocErrorState('Login failed, wrong password'));
      return;
    }

    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    await sharedPreferences.setBool("is_logged_in", true);

    emit(AuthBlocLoggedInState());
  }

  void createUser(User user) async {
    emit(AuthBlocLoadingState());
    final insert = await DBHelper.insert('user_auth', user.toJson());
    if (insert == 0) {
      emit(AuthBlocErrorState('User with this email already registered'));
      return;
    }

    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    await sharedPreferences.setBool("is_logged_in", true);

    emit(AuthBlocLoggedInState());
  }

  Future<void> logOut() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    await sharedPreferences.clear();

    emit(AuthBlocInitialState());
  }
}
