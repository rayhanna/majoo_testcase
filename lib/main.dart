import 'package:google_fonts/google_fonts.dart';
import 'package:majootestcase/ui/home/home_bloc_screen.dart';
import 'package:majootestcase/ui/login/login_screen.dart';
import 'package:majootestcase/utils/constant.dart';
import 'bloc/auth_bloc/auth_bloc_cubit.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'bloc/home_bloc/home_bloc_cubit.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widgets is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Majoo Movie',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.orange,
        scaffoldBackgroundColor: ColorTheme.backgroundColor,
        visualDensity: VisualDensity.adaptivePlatformDensity,
        textTheme: GoogleFonts.poppinsTextTheme(
          Theme.of(context).textTheme,
        ).apply(
          bodyColor: Colors.white,
          displayColor: Colors.white,
        ),
      ),
      home: BlocProvider(
        create: (context) => AuthBlocCubit()..fetchHistoryLogin(),
        child: MyHomePageScreen(),
      ),
    );
  }
}

class MyHomePageScreen extends StatelessWidget {
  const MyHomePageScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AuthBlocCubit, AuthBlocState>(
      builder: (context, state) {
        if (state is AuthBlocLoginState) {
          return LoginScreen();
        } else if (state is AuthBlocLoggedInState) {
          return BlocProvider(
            create: (context) => HomeBlocCubit()..fetchingData(),
            child: HomeBlocScreen(),
          );
        }

        return Scaffold();
      },
    );
  }
}
