import 'dart:async';
import 'package:dio/dio.dart';
import 'package:pretty_dio_logger/pretty_dio_logger.dart';

Dio? dioInstance;
createInstance() async {
  var options = BaseOptions(
    baseUrl: "https://imdb8.p.rapidapi.com",
    queryParameters: {'q': 'game of thr'},
    connectTimeout: 12000,
    receiveTimeout: 12000,
    headers: {
      'x-rapidapi-host': 'imdb8.p.rapidapi.com',
      'x-rapidapi-key': '99beea8ad7msh117481c92940227p19dacfjsndea6da06558a'
    },
  );
  dioInstance = Dio(options);
  dioInstance!.interceptors.add(PrettyDioLogger(
      requestHeader: true,
      requestBody: true,
      responseBody: true,
      responseHeader: false,
      error: true,
      compact: true,
      maxWidth: 90));
}

Future<Dio?> dio() async {
  await createInstance();
  return dioInstance;
}
